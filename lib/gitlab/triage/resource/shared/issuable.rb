# frozen_string_literal: true

require_relative '../label'
require_relative '../label_event'
require_relative '../milestone'

module Gitlab
  module Triage
    module Resource
      module Shared
        module Issuable
          def milestone
            @milestone ||=
              resource[:milestone] &&
              Milestone.new(resource[:milestone], parent: self)
          end

          # This will be more useful when we have:
          # https://gitlab.com/gitlab-org/gitlab-ce/issues/51011
          def labels
            @labels ||= resource[:labels] # an array of label names
              .map { |label| Label.new({ name: label }, parent: self) }
          end

          # Make this an alias of `labels` when we have:
          # https://gitlab.com/gitlab-org/gitlab-ce/issues/51011
          def labels_with_details
            # Labels can be deleted thus event.label can be nil
            @labels_with_details ||= label_events
              .select { |event| event.action == 'add' && event.label }
              .map(&:label)
              .uniq(&:name)
              .select { |label| resource[:labels].include?(label.name) }
          end

          def label_events
            @label_events ||= query_label_events
              .map { |label_event| LabelEvent.new(label_event, parent: self) }
          end

          def labels_chronologically
            @labels_chronologically ||= labels_with_details.sort_by(&:added_at)
          end

          private

          def query_label_events
            network.query_api_cached(
              resource_url(sub_resource_type: 'resource_label_events'))
          end
        end
      end
    end
  end
end
