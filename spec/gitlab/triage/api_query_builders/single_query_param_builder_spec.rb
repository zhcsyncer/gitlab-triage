require 'spec_helper'

require 'gitlab/triage/api_query_builders/single_query_param_builder'

describe Gitlab::Triage::APIQueryBuilders::SingleQueryParamBuilder do
  let(:single_param) do
    {
      'state' => 'closed',
      'milestone' => 'v10.0'
    }
  end

  context '#build_param' do
    it 'builds the correct search string' do
      single_param.each do |k, v|
        builder = described_class.new(k, v)

        expect(builder.build_param).to eq("&#{k}=#{v}")
      end
    end

    context 'with leading or trailing whitespaces' do
      let(:single_param) do
        {
          'state' => ' closed '
        }
      end

      it 'strips the leading and trailing whitespaces' do
        single_param.each do |k, v|
          builder = described_class.new(k, v)

          expect(builder.build_param).to eq("&#{k}=#{v.strip}")
        end
      end
    end
  end
end
